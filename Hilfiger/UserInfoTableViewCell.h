//
//  UserInfoTableViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

FOUNDATION_EXPORT CGFloat const kUserInfoTableViewCellHeight;

#import "BaseTableViewCell.h"

@interface UserInfoTableViewCell : BaseTableViewCell

@property (nonatomic, strong) UIView *vSeparator;
@property (nonatomic, strong) UILabel *lblDesc;
@property (nonatomic, strong) UILabel *lblData;

@end
