//
//  AppDelegate.h
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

