//
//  UserInfoTableViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "UserInfoTableViewCell.h"
#import "UIView+Utils.h"

CGFloat const kUserInfoTableViewCellHeight = 50.;

@implementation UserInfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor themeColorBackground];
        
        _vSeparator = [UIView new];
        _vSeparator.backgroundColor = COLOR(0xc4c4c4);
        [self.contentView addSubview:_vSeparator];
        
        _lblDesc = [UILabel new];
        _lblDesc.font = [UIFont themeFontDesc];
        _lblDesc.textColor = [UIColor themeColorTextLight];
        [self.contentView addSubview:_lblDesc];
        
        _lblData = [UILabel new];
        _lblData.font = [UIFont themeFontData];
        _lblData.textColor = [UIColor themeColorText];
        [self.contentView addSubview:_lblData];
        
        NSDictionary *alMetrics = @{@"kOnePixelHeight": @([UIView onePixelLine])};
        
        ALVariableBindingsAM(_vSeparator, _lblDesc, _lblData);
        
        AL_Visual_AddToView_M(self.contentView, @"V:[_vSeparator(kOnePixelHeight)]-0-|", alMetrics);
        AL_Visual_AddToView(self.contentView, @"H:|-35-[_vSeparator]-35-|");
        
        AL_Visual_AddToView(self.contentView, @"H:|-35-[_lblDesc]");
        AL_CenterY_AddToView(self.contentView, _lblDesc, self.contentView);
        
        AL_Visual_AddToView(self.contentView, @"H:[_lblData]");
        AL_CenterX_AddToView(self.contentView, _lblData, self.contentView);
        AL_CenterY_AddToView(self.contentView, _lblData, self.contentView);
        
        [self layoutIfNeeded];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _lblDesc.text = nil;
    _lblData.text = nil;
}


@end
