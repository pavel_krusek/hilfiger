//
//  VoucherListViewController.h
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "BaseViewController.h"

@interface VoucherListViewController : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSettings;

@end
