//
//  VoucherDetailViewController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "VoucherDetailViewController.h"

@implementation VoucherDetailViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lblDate.font = [UIFont themeFontDetailDate];
    _lblDate.textColor = [UIColor themeColorText];
    _lblDate.text = [NSString stringWithFormat:LOCS(@"VALID FOR %@"), _voucher.date];
    
    _lblName.font = [UIFont themeFontDetailName];
    _lblName.textColor = [UIColor themeColorText];
    _lblName.text = @"HI HOGAN,";
    
    _lblText1.font = [UIFont themeFontDetailText1];
    _lblText1.textColor = [UIColor themeColorText];
    _lblText1.text = @"Welcome to the Hilfiger Club!\nAs welcome present you receive";
    
    _lblText2.font = [UIFont themeFontDetailText2];
    _lblText2.textColor = [UIColor themeColorText];
    _lblText2.text = _voucher.desc;
    
    [_btnSave setTitle:LOCS(@"SAVE VOUCHER") forState:UIControlStateNormal];
    _btnSave.backgroundColor = [UIColor themeColorGreen];
    _btnSave.titleLabel.font = [UIFont themeFontButton];
    _btnSave.adjustsImageWhenHighlighted = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

#pragma mark - Actions

- (IBAction)actionSave:(id)sender {
    
}


@end
