//
//  LoginViewController.h
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
