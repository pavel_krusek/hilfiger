//
//  LoginButtonsTableViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "LoginButtonsTableViewCell.h"

CGFloat const kLoginButtonsTableViewCellHeight = 330.;

@implementation LoginButtonsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor themeColorBackground];
        
        _btnLogin = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnLogin setTitle:LOCS(@"SIGN IN") forState:UIControlStateNormal];
        _btnLogin.backgroundColor = [UIColor themeColorGreen];
        _btnLogin.titleLabel.font = [UIFont themeFontButton];
        _btnLogin.adjustsImageWhenHighlighted = NO;
        [_btnLogin addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnLogin];
        
        NSMutableAttributedString *forgotString = [[NSMutableAttributedString alloc] initWithString:@"Forgot your password?"];
        [forgotString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [forgotString length])];
        
        _btnForgot = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnForgot setAttributedTitle:forgotString forState:UIControlStateNormal];
        _btnForgot.titleLabel.font = [UIFont themeFontSubtitle];
        [_btnForgot setTitleColor:[UIColor themeColorTextLight] forState:UIControlStateNormal];
        _btnForgot.adjustsImageWhenHighlighted = NO;
        [_btnForgot addTarget:self action:@selector(forgot:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnForgot];
        
        _ivOr = [UIImageView new];
        _ivOr.image = [UIImage imageNamed:@"divider"];
        [self.contentView addSubview:_ivOr];
        
        _btnFacebook = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnFacebook setTitle:LOCS(@"SIGN IN WITH FACEBOOK") forState:UIControlStateNormal];
        [_btnFacebook setBackgroundImage:[[UIImage imageNamed:@"btnFb"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 50, 0, 50)] forState:UIControlStateNormal];
        [_btnFacebook setTitleColor:[UIColor themeColorGreen] forState:UIControlStateNormal];
        _btnFacebook.titleLabel.font = [UIFont themeFontDetailDate];
        _btnFacebook.adjustsImageWhenHighlighted = NO;
        [_btnFacebook addTarget:self action:@selector(facebook:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnFacebook];
        
        _btnRegister = [UIButton buttonWithType:UIButtonTypeCustom];
        [_btnRegister setTitle:LOCS(@"NEW CUSTOMER? REGISTER HERE.") forState:UIControlStateNormal];
        _btnRegister.backgroundColor = [UIColor themeColorText];
        _btnRegister.titleLabel.font = [UIFont themeFontButton];
        _btnRegister.adjustsImageWhenHighlighted = NO;
        [_btnRegister addTarget:self action:@selector(registerUser:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnRegister];
      
        ALVariableBindingsAM(_btnLogin, _btnForgot, _ivOr, _btnFacebook, _btnRegister);
        
        AL_Visual_AddToView(self.contentView, @"V:|-15-[_btnLogin(48)]-15-[_btnForgot(48)]-15-[_ivOr]-25-[_btnFacebook(48)]-30-[_btnRegister(60)]");
        AL_Visual_AddToView(self.contentView, @"H:|-30-[_btnLogin]-30-|");
        AL_Visual_AddToView(self.contentView, @"H:|-30-[_btnForgot]-30-|");
        AL_Visual_AddToView(self.contentView, @"H:|-[_ivOr]-|");
        AL_Visual_AddToView(self.contentView, @"H:|-30-[_btnFacebook]-30-|");
        AL_Visual_AddToView(self.contentView, @"H:|[_btnRegister]|");
        
        [self layoutIfNeeded];
    }
    
    return self;
}

- (void)login:(id)sender {
    if (_buttonLoginHandler) {
        _buttonLoginHandler(self);
    }
}

- (void)forgot:(id)sender {
    if (_buttonForgotHandler) {
        _buttonForgotHandler(self);
    }
}

- (void)facebook:(id)sender {
    if (_buttonFacebookHandler) {
        _buttonFacebookHandler(self);
    }
}

- (void)registerUser:(id)sender {
    if (_buttonRegisterHandler) {
        _buttonRegisterHandler(self);
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _buttonLoginHandler = nil;
    _buttonFacebookHandler = nil;
    _buttonForgotHandler = nil;
    _buttonRegisterHandler = nil;
}


@end
