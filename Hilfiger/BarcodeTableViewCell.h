//
//  BarcodeTableViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

FOUNDATION_EXPORT CGFloat const kBarcodeTableViewCellHeight;

#import "BaseTableViewCell.h"

@interface BarcodeTableViewCell : BaseTableViewCell

@property (nonatomic, strong) UIImageView *ivStripe;
@property (nonatomic, strong) UIView *vBarcodeContainer;
@property (nonatomic, strong) UIImageView *ivBarcode;

@end
