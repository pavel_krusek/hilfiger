//
//  BaseFormTableViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "BaseFormTableViewCell.h"

@implementation BaseFormTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _textField = [UITextField new];
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _textFieldDidBeginEditingHandler = nil;
    _textFieldDidChangeHandler = nil;
    _textFieldDidEndEditingHandler = nil;
    _textFieldShouldReturnHandler = nil;
    _textFieldShouldBeginEditingHandler = nil;
    
    _textField.placeholder = nil;
    _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _textField.text = nil;
}

#pragma mark - UITextField delegate's methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (_textFieldShouldReturnHandler) {
        _textFieldShouldReturnHandler(self);
        return NO;
    } else {
        [textField resignFirstResponder];
        return YES;
    }
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (_textFieldDidChangeHandler) {
        _textFieldDidChangeHandler(self, textField);
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (_textFieldDidBeginEditingHandler) {
        _textFieldDidBeginEditingHandler(self);
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (_textFieldDidEndEditingHandler) {
        _textFieldDidEndEditingHandler(self, textField);
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (_textFieldShouldBeginEditingHandler) {
        _textFieldShouldBeginEditingHandler(self);
        return NO;
    }
    return YES;
}


@end
