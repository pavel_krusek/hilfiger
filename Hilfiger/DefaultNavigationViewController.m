//
//  DefaultNavigationViewController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "DefaultNavigationViewController.h"

@implementation DefaultNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = self.navigationBar.frame;
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:frame];
    logoView.contentMode = UIViewContentModeCenter;
    logoView.image = [UIImage imageNamed:@"logo"];

    [self.navigationBar addSubview:logoView];
}

@end
