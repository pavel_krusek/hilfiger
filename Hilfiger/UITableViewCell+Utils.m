//
//  UITableViewCell+Utils.m
//  njoy-ios
//
//  Created by Pavel Krusek on 10/21/14.
//  Copyright (c) 2014 Lithio. All rights reserved.
//

#import "UITableViewCell+Utils.h"

@implementation UITableViewCell (Utils)

+ (NSString *)defaultReuseIdentifier{
    return NSStringFromClass(self);
}

@end
