//
//  VoucherListViewController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "VoucherListViewController.h"
#import "VoucherDetailViewController.h"

#import "VoucherCollectionViewCell.h"

#import "UICollectionViewCell+Utils.h"

static NSString *const kSeguePushVoucherDetail = @"kSeguePushVoucherDetail";

@implementation VoucherListViewController {
    NSArray *_vouchers;
}

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = _btnSettings;
    
    [_collectionView registerClass:[VoucherCollectionViewCell class] forCellWithReuseIdentifier:[VoucherCollectionViewCell defaultReuseIdentifier]];
    
    _collectionView.backgroundColor = [UIColor themeColorBackground];

    _vouchers = @[[Voucher voucherWithImagesource:@"temp1.jpg" description:@"Voucher 1 and some text here" date:@"date 1"],
                  [Voucher voucherWithImagesource:@"temp2.jpg" description:@"Voucher 2 great discount for your purposes" date:@"date 2"],
                  [Voucher voucherWithImagesource:@"temp3.jpg" description:@"Voucher 3 some other text long test tail on four rows but must be three rows" date:@"date 3"],
                  [Voucher voucherWithImagesource:@"temp4.jpg" description:@"Voucher 4" date:@"date 4"],
                  [Voucher voucherWithImagesource:@"temp5.jpg" description:@"Voucher 5" date:@"date 5"],
                  [Voucher voucherWithImagesource:@"temp6.jpg" description:@"Voucher 6" date:@"date 6"],
                  [Voucher voucherWithImagesource:@"temp7.jpg" description:@"Voucher 7" date:@"date 7"],
                  [Voucher voucherWithImagesource:@"temp8.jpg" description:@"Voucher 8" date:@"date 8"]
                  ];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self layoutForSize:self.view.bounds.size];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"temp_login"] == 0) {
        UINavigationController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:controller animated:NO completion:nil];
    }
}

#pragma mark - Private

- (void)layoutForSize:(CGSize)size {
    NSInteger cellSize = (size.width - 18) / 2;
    
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)_collectionView.collectionViewLayout;
    
    CGFloat paddings = MAX(((int)size.width % cellSize) - ((int)size.width / cellSize) - 5, 0);
    
    flowLayout.sectionInset = UIEdgeInsetsMake(0, paddings / 2, paddings / 2, paddings / 2);
    flowLayout.itemSize = CGSizeMake(cellSize, kVoucherCollectionViewCellCollectionViewCellHeight);
    flowLayout.minimumInteritemSpacing = 6.;
    flowLayout.minimumLineSpacing = 6.;
    
    [_collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark - CollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_vouchers count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    VoucherCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[VoucherCollectionViewCell defaultReuseIdentifier] forIndexPath:indexPath];
    
    Voucher *voucher = _vouchers[indexPath.row];
    
    cell.ivImage.image = voucher.iVoucher;
    cell.lblSubtitle.text = voucher.desc;
    cell.lblDate.text = voucher.date;
    
    return cell;
}

#pragma mark - CollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Voucher *voucher = _vouchers[indexPath.row];
    [self performSegueWithIdentifier:kSeguePushVoucherDetail sender:voucher];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    WhenSegueIdentifierDo(kSeguePushVoucherDetail, ^{
        VoucherDetailViewController *controller = segue.destinationViewController;
        controller.voucher = sender;
        controller.hidesBottomBarWhenPushed = YES;
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LOCS(@"") style:UIBarButtonItemStylePlain target:nil action:nil];
    });
}


@end
