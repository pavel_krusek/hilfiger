//
//  TextFieldTableViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "TextFieldTableViewCell.h"

CGFloat const kTextFieldTableViewCellHeight = 60.;

@implementation TextFieldTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor themeColorBackground];
        
        self.textField.font = [UIFont themeFontData];
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.textColor = [UIColor themeColorText];
        self.textField.backgroundColor = [UIColor whiteColor];
        self.textField.layer.borderWidth = 1;
        self.textField.layer.borderColor = [COLOR(0xd4d4d4) CGColor];
        self.textField.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.textField];
        
        UITextField *textField = self.textField;
        
        ALVariableBindingsAM(textField);
        
        AL_Visual_AddToView(self.contentView, @"V:[textField(48)]");
        
        AL_Visual_AddToView(self.contentView, @"H:|-30-[textField]-30-|");
        
        AL_CenterY_AddToView(self.contentView, textField, self.contentView);
        
        [self layoutIfNeeded];
    }
    
    return self;
}


@end
