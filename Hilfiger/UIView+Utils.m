//
//  UIView+Utils.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)

+ (CGFloat)onePixelLine {
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGFloat onePixel = 1.0 / mainScreen.scale;
    if ([mainScreen respondsToSelector:@selector(nativeScale)]) {
        onePixel = 1.0 / mainScreen.nativeScale;
    }
    return onePixel;
}


@end
