//
//  LoginButtonsTableViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

FOUNDATION_EXPORT CGFloat const kLoginButtonsTableViewCellHeight;

#import "BaseTableViewCell.h"

@interface LoginButtonsTableViewCell : BaseTableViewCell

@property (nonatomic, strong) UIButton *btnLogin;
@property (nonatomic, strong) UIButton *btnFacebook;
@property (nonatomic, strong) UIButton *btnForgot;
@property (nonatomic, strong) UIButton *btnRegister;
@property (nonatomic, strong) UIImageView *ivOr;

@property (nonatomic, copy) void (^buttonLoginHandler)(LoginButtonsTableViewCell *cell);
@property (nonatomic, copy) void (^buttonFacebookHandler)(LoginButtonsTableViewCell *cell);
@property (nonatomic, copy) void (^buttonForgotHandler)(LoginButtonsTableViewCell *cell);
@property (nonatomic, copy) void (^buttonRegisterHandler)(LoginButtonsTableViewCell *cell);

@end
