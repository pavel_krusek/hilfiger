//
//  VoucherCollectionViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

FOUNDATION_EXPORT CGFloat const kVoucherCollectionViewCellCollectionViewCellHeight;

@interface VoucherCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *ivImage;
@property (nonatomic, strong) UILabel *lblSubtitle;
@property (nonatomic, strong) UILabel *lblDate;
@property (nonatomic, strong) UIImageView *ivTime;

@end
