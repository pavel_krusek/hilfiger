//
//  TitleTableViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

FOUNDATION_EXPORT CGFloat const kTitleTableViewCellHeight;

#import "BaseTableViewCell.h"

@interface TitleTableViewCell : BaseTableViewCell

@property (nonatomic, strong) UILabel *lblTitle;

@end
