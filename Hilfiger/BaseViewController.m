//
//  BaseViewController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "BaseViewController.h"

@implementation BaseViewController

#pragma mark - Lifecycle

- (void)dealloc {
    NSLog(@"dealloc %@", self);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor themeColorBackground];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (IBAction)actionSettings:(id)sender {
    NSLog(@"settings");
}


@end
