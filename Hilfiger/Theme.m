//
//  Theme.m
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "Theme.h"

#import "UIImage+Utils.h"

NSString *const ThemeFontDefaultRegular = @"GillSans";
NSString *const ThemeFontDefaultLight = @"GillSans-Light";
NSString *const ThemeFontDefaultBold = @"GillSans-Bold";

@implementation Theme

+ (void)appearance {
    [[UINavigationBar appearance] setBarTintColor:[UIColor themeColorText]];
    [[UINavigationBar appearance] setTintColor:[UIColor themeColorText]];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor themeColorBackground]] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];
    
    [[UITabBar appearance] setBackgroundImage:[UIImage new]];
    [[UITabBar appearance] setShadowImage:[UIImage new]];
    [[UITabBar appearance] setBackgroundColor:[UIColor themeColorBackgroundDark]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont themeFontTabBar]} forState:UIControlStateNormal];
}


@end



@implementation UIFont (ThemeFont)

+ (UIFont *)themeFontTitle {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:35.];
}

+ (UIFont *)themeFontTabBar {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:12.];
}

+ (UIFont *)themeFontButton {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:18.];
}

+ (UIFont *)themeFontSubtitle {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:15.];
}

+ (UIFont *)themeFontDesc {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:12.];
}

+ (UIFont *)themeFontData {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:16.];
}

+ (UIFont *)themeFontSmall {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:10.];
}

+ (UIFont *)themeFontDetailDate {
    return [UIFont fontWithName:ThemeFontDefaultRegular size:14.];
}

+ (UIFont *)themeFontDetailName{
    return [UIFont fontWithName:ThemeFontDefaultBold size:20.];
}

+ (UIFont *)themeFontDetailText1 {
    return [UIFont fontWithName:ThemeFontDefaultLight size:16.];
}

+ (UIFont *)themeFontDetailText2 {
    return [UIFont fontWithName:ThemeFontDefaultBold size:16.];
}

@end

@implementation UIColor (ThemeColor)

+ (UIColor *)themeColorBackground {
    return COLOR(0xe6e7e8);
}

+ (UIColor *)themeColorBackgroundDark {
    return COLOR(0x141821);
}

+ (UIColor *)themeColorText {
    return COLOR(0x00154d);
}

+ (UIColor *)themeColorGreen {
    return COLOR(0x17995a);
}

+ (UIColor *)themeColorTextLight {
    return COLOR(0x515f84);
}

@end

