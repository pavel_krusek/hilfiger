//
//  HilfigerTabBarController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "HilfigerTabBarController.h"

@implementation HilfigerTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *unselectedImages = @[@"menu-ico-1", @"menu-ico-2", @"menu-ico-3"];
    NSArray *selectedImages = @[@"menu-ico-1a", @"menu-ico-2a", @"menu-ico-3a"];
    NSArray *titles = @[LOCS(@"VOUCHERS"), LOCS(@"CLUB CARD"), LOCS(@"STORES")];
    
    NSArray *items = self.tabBar.items;
    
    [items enumerateObjectsUsingBlock:^(UITabBarItem *item, NSUInteger idx, BOOL *stop) {
        item.image = [UIImage imageNamed:unselectedImages[idx]];
        item.selectedImage = [UIImage imageNamed:selectedImages[idx]];
        item.title = titles[idx];
        
        item.titlePositionAdjustment = UIOffsetMake(0., -3.);
        item.imageInsets = UIEdgeInsetsMake(-3, 0, 3, 0);
    }];
}


@end
