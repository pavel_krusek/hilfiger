//
//  LoginHeaderTableViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "LoginHeaderTableViewCell.h"

CGFloat const kLoginHeaderTableViewCellHeight = 122.;

@implementation LoginHeaderTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor themeColorBackground];
        
        _lblTitle = [UILabel new];
        _lblTitle.font = [UIFont fontWithName:@"Caslon540ITALIC" size:48];
        _lblTitle.textColor = [UIColor themeColorText];
        _lblTitle.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_lblTitle];
        
        _lblSubTitle = [UILabel new];
        _lblSubTitle.font = [UIFont themeFontTitle];
        _lblSubTitle.textColor = [UIColor themeColorGreen];
        _lblSubTitle.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_lblSubTitle];
        
        ALVariableBindingsAM(_lblTitle, _lblSubTitle);
        
        AL_Visual_AddToView(self.contentView, @"V:|-20-[_lblTitle]-0-[_lblSubTitle]");
        AL_Visual_AddToView(self.contentView, @"H:|[_lblTitle]|");
        AL_Visual_AddToView(self.contentView, @"H:|[_lblSubTitle]|");
        
        [self layoutIfNeeded];
    }
    
    return self;
}


@end
