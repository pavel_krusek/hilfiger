//
//  VoucherDetailViewController.h
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "BaseViewController.h"

@interface VoucherDetailViewController : BaseViewController

@property (nonatomic, weak) IBOutlet UIImageView *ivVisual;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblText1;
@property (nonatomic, weak) IBOutlet UILabel *lblText2;
@property (nonatomic, weak) IBOutlet UIButton *btnSave;

@property (nonatomic, strong) Voucher *voucher;

- (IBAction)actionSave:(id)sender;

@end
