//
//  LoginHeaderTableViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

FOUNDATION_EXPORT CGFloat const kLoginHeaderTableViewCellHeight;

#import "BaseTableViewCell.h"

@interface LoginHeaderTableViewCell : BaseTableViewCell

@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) UILabel *lblSubTitle;

@end
