//
//  UIImage+Utils.m
//  Reporterio
//
//  Created by Pavel Krusek on 11/15/14.
//  Copyright (c) 2014 Pavel Krusek. All rights reserved.
//

#import "UIImage+Utils.h"

@implementation UIImage (Utils)

#pragma mark - Generic theming methods

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGFloat onePixel = 1.0 / [[UIScreen mainScreen] scale];
    return [UIImage imageWithColor:color andSize:CGSizeMake(onePixel, onePixel)];
}

+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    
    // Create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 2);
    [color setFill];
    
    UIRectFill(rect);   // Fill it with your color
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
