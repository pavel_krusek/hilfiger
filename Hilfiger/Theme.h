//
//  Theme.h
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

extern NSString *const ThemeFontDefaultRegular;
extern NSString *const ThemeFontDefaultLight;
extern NSString *const ThemeFontDefaultBold;

@interface Theme : NSObject

+ (void)appearance;

@end


@interface UIFont (ThemeFont)

+ (UIFont *)themeFontTitle;
+ (UIFont *)themeFontTabBar;
+ (UIFont *)themeFontButton;
+ (UIFont *)themeFontSubtitle;
+ (UIFont *)themeFontSmall;
+ (UIFont *)themeFontDetailDate;
+ (UIFont *)themeFontDetailName;
+ (UIFont *)themeFontDetailText1;
+ (UIFont *)themeFontDetailText2;
+ (UIFont *)themeFontDesc;
+ (UIFont *)themeFontData;

@end


@interface UIColor (ThemeColor)

+ (UIColor *)themeColorBackground;
+ (UIColor *)themeColorBackgroundDark;
+ (UIColor *)themeColorGreen;
+ (UIColor *)themeColorText;
+ (UIColor *)themeColorTextLight;

@end