//
//  UICollectionViewCell+Utils.h
//  Inviter
//
//  Created by Pavel Krusek on 13/01/15.
//
//

@interface UICollectionViewCell (Utils)

+ (NSString *)defaultReuseIdentifier;

@end
