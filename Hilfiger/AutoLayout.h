//
//  AutoLayout.h
//  njoy-ios
//
//  Created by Pavel Krusek on 10/21/14.
//  Copyright (c) 2014 Lithio. All rights reserved.
//

#pragma mark - Variable Bindings

// Improved NSDictionaryOfVariableBindings which does not stop on nil object. It allows to set translatesAutoresizingMaskIntoConstraints or clear constraints for all views in args.
static inline NSDictionary *_ALDictionaryOfVariableBindings(BOOL autoresizingMaskOff, BOOL clearConstraints, NSString *commaSeparatedKeysString, id firstValue, ...) {
    NSArray *keys = [commaSeparatedKeysString componentsSeparatedByString:@", "];
    NSMutableDictionary *dictionary = @{}.mutableCopy;
    
    if (keys.count > 0) {
        va_list args;
        va_start(args, firstValue);
        
        for (NSUInteger i = 0, l = keys.count; i < l; i++) {
            id obj = (i == 0) ? firstValue : va_arg(args, id);
            
            if (obj && [obj isKindOfClass:[UIView class]]) {
                dictionary[keys[i]] = obj;
                
                if (autoresizingMaskOff) {
                    [(UIView *)obj setTranslatesAutoresizingMaskIntoConstraints:NO];
                }
                if (clearConstraints) {
                    [obj removeConstraints:[obj constraints]];
                }
            }
        }
        
        va_end(args);
    }
    
    return [dictionary copy];
}

// Creates auto layout bindings dictionary without modified 'translatesAutoresizingMaskIntoConstraints' or clearing constraints.
#define ALVariableBindings(...) NSDictionary *__vb = _ALDictionaryOfVariableBindings(NO, NO,  @"" # __VA_ARGS__, __VA_ARGS__, nil)

// Create auto layout bindings dictionary without modified 'translatesAutoresizingMaskIntoConstraints' and cleared constraints which comes from IB.
#define ALVariableBindingsCC(...) NSDictionary *__vb = _ALDictionaryOfVariableBindings(NO, YES, @"" # __VA_ARGS__, __VA_ARGS__, nil)

// Creates auto layout bindings dictionary with 'translatesAutoresizingMaskIntoConstraints' set to NO.
#define ALVariableBindingsAM(...) NSDictionary *__vb = _ALDictionaryOfVariableBindings(YES, NO,  @"" # __VA_ARGS__, __VA_ARGS__, nil)

#pragma mark - Visual

#define AL_Visual_Create(__visual) _al_visual_create((__visual), 0, nil)
#define AL_Visual_Create_O(__visual, __options) _al_visual_create((__visual), (__options), nil)
#define AL_Visual_Create_M(__visual, __metrics) _al_visual_create((__visual), 0, (__metrics))
#define AL_Visual_Create_OM(__visual, __options, __metrics) _al_visual_create((__visual), (__options), (__metrics))
#define _al_visual_create(vs, o, m) [NSLayoutConstraint constraintsWithVisualFormat:(vs) options:(o) metrics:(m) views:__vb]

#define AL_Visual_AddToView(__view, __visual) _al_visual_addtoview((__view), (__visual), 0, nil)
#define AL_Visual_AddToView_O(__view, __visual, __options) _al_visual_addtoview((__view), (__visual), (__options), nil)
#define AL_Visual_AddToView_M(__view, __visual, __metrics) _al_visual_addtoview((__view), (__visual), 0, (__metrics))
#define AL_Visual_AddToView_OM(__view, __visual, __options, metrics) _al_visual_addtoview((__view), (__visual), (__options), (__metrics))
#define _al_visual_addtoview(vw, vs, o, m) [(vw) addConstraints:_al_visual_create((vs), (o), (m))]

#define AL_Visual_AddToSelf(__visual) _al_visual_addtoview(self, (__visual), 0, nil)
#define AL_Visual_AddToSelf_O(__visual, __options) _al_visual_addtoview(self, (__visual), (__options), nil)
#define AL_Visual_AddToSelf_M(__visual, __metrics) _al_visual_addtoview(self, (__visual), 0, (__metrics))
#define AL_Visual_AddToSelf_OM(__visual, __options, metrics) _al_visual_addtoview(self, (__visual), (__options), (__metrics))

#define AL_Visual_AddToArray(__array, __visual) _al_visual_addtoarray((__array), (__visual), 0, nil)
#define AL_Visual_AddToArray_O(__array, __visual, __options) _al_visual_addtoarray((__array), (__visual), (__options), nil)
#define AL_Visual_AddToArray_M(__array, __visual, __metrics) _al_visual_addtoarray((__array), (__visual), 0, (__metrics))
#define AL_Visual_AddToArray_OM(__array, __visual, __options, metrics) _al_visual_addtoarray((__array), (__visual), (__op
#define _al_visual_addtoarray(ar, vs, o, m) [(ar) addObjectsFromArray:_al_visual_create((vs), (o), (m))]

#pragma mark - Attributes

#define AL_Attr_Create(__item, __toItem, __attribute, __relation) _al_attr_create((__item), (__toItem), (__attribute), (__relation), 1., 0)
#define AL_Attr_Create_M(__item, __toItem, __attribute, __relation, __multiplier) _al_attr_create((__item), (__toItem), (__attribute), (__relation), (__multiplier), 0)
#define AL_Attr_Create_C(__item, __toItem, __attribute, __relation, __constant) _al_attr_create((__item), (__toItem), (__attribute), (__relation), 1., (__constant))
#define AL_Attr_Create_MC(__item, __toItem, __attribute, __relation, __multiplier, __constant) _al_attr_create((__item), (__toItem), (__attribute), (__relation), (__multiplier), (__constant))
#define _al_attr_create(a, b, attr, r, m, c) [NSLayoutConstraint constraintWithItem:(a) attribute:(attr) relatedBy:(r) toItem:(b) attribute:(attr) multiplier:(m) constant:(c)]

#define AL_Attr_AddToView(__view, __item, __toItem, __attribute, __relation) _al_attr_addtoview((__view), (__item), (__toItem), (__attribute), (__relation), 1., 0)
#define AL_Attr_AddToView_M(__view, __item, __toItem, __attribute, __relation, __multiplier) _al_attr_addtoview((__view), (__item), (__toItem), (__attribute), (__relation), (__multiplier), 0)
#define AL_Attr_AddToView_C(__view, __item, __toItem, __attribute, __relation, __constant) _al_attr_addtoview((__view), (__item), (__toItem), (__attribute), (__relation), 1., (__constant))
#define AL_Attr_AddToView_MC(__view, __item, __toItem, __attribute, __relation, __multiplier, __constant) _al_attr_addtoview((__view), (__item), (__toItem), (__attribute), (__relation), (__multiplier), (__constant))
#define _al_attr_addtoview(vw, a, b, attr, r, m, c) [(vw) addConstraint:_al_attr_create((a), (b), (attr), (r), (m), (c))]

#define AL_Attr_AddToSelf(__item, __toItem, __attribute, __relation) _al_attr_addtoview(self, (__item), (__toItem), (__attribute), (__relation), 1., 0)
#define AL_Attr_AddToSelf_M(__item, __toItem, __attribute, __relation, __multiplier) _al_attr_addtoview(self, (__item), (__toItem), (__attribute), (__relation), (__multiplier), 0)
#define AL_Attr_AddToSelf_C(__item, __toItem, __attribute, __relation, __constant) _al_attr_addtoview(self, (__item), (__toItem), (__attribute), (__relation), 1., (__constant))
#define AL_Attr_AddToSelf_MC(__item, __toItem, __attribute, __relation, __multiplier, __constant) _al_attr_addtoview(self, (__item), (__toItem), (__attribute), (__relation), (__multiplier), (__constant))

#define AL_Attr_AddToArray(__array, __item, __toItem, __attribute, __relation) _al_attr_addtoarray((__array), (__item), (__toItem), (__attribute), (__relation), 1., 0)
#define AL_Attr_AddToArray_M(__array, __item, __toItem, __attribute, __relation, __multiplier) _al_attr_addtoarray((__array), (__item), (__toItem), (__attribute), (__relation), (__multiplier), 0)
#define AL_Attr_AddToArray_C(__array, __item, __toItem, __attribute, __relation, __constant) _al_attr_addtoarray((__array), (__item), (__toItem), (__attribute), (__relation), 1., (__constant))
#define AL_Attr_AddToArray_MC(__array, __item, __toItem, __attribute, __relation, __multiplier, __constant) _al_attr_addtoarray((__array), (__item), (__toItem), (__attribute), (__relation), (__multiplier), (__constant))
#define _al_attr_addtoarray(ar, a, b, attr, r, m, c) [(ar) addObject:_al_attr_create((a), (b), (attr), (r), (m), (c))]

#pragma mark - Centering

#define AL_CenterX_Create(__item, __toItem) AL_Attr_Create((__item), (__toItem), NSLayoutAttributeCenterX, NSLayoutRelationEqual)

#define AL_CenterX_AddToView(__view, __item, __toItem) [(__view) addConstraint:AL_CenterX_Create((__item), (__toItem))]
#define AL_CenterX_AddToSelf(__item, __toItem) [self addConstraint:AL_CenterX_Create((__item), (__toItem))]
#define AL_CenterX_AddToArray(__array, __item, __toItem) [(__array) addObject:AL_CenterX_Create((__item), (__toItem))]

#define AL_CenterY_Create(__item, __toItem) AL_Attr_Create((__item), (__toItem), NSLayoutAttributeCenterY, NSLayoutRelationEqual)

#define AL_CenterY_AddToView(__view, __item, __toItem) [(__view) addConstraint:AL_CenterY_Create((__item), (__toItem))]
#define AL_CenterY_AddToSelf(__item, __toItem) [self addConstraint:AL_CenterY_Create((__item), (__toItem))]
#define AL_CenterY_AddToArray(__array, __item, __toItem) [(__array) addObject:AL_CenterY_Create((__item), (__toItem))]

