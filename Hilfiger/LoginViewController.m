//
//  LoginViewController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "LoginViewController.h"

#import "LoginButtonsTableViewCell.h"
#import "TextFieldTableViewCell.h"
#import "LoginHeaderTableViewCell.h"

#import "UITableViewCell+Utils.h"

@implementation LoginViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView.backgroundColor = [UIColor themeColorText];
    
    UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0,-480,_tableView.frame.size.width,480)];
    topview.backgroundColor = [UIColor themeColorBackground];
    [_tableView addSubview:topview];
    
    [_tableView registerClass:[LoginButtonsTableViewCell class] forCellReuseIdentifier:[LoginButtonsTableViewCell defaultReuseIdentifier]];
    [_tableView registerClass:[TextFieldTableViewCell class] forCellReuseIdentifier:[TextFieldTableViewCell defaultReuseIdentifier]];
    [_tableView registerClass:[LoginHeaderTableViewCell class] forCellReuseIdentifier:[LoginHeaderTableViewCell defaultReuseIdentifier]];

}

#pragma mark - Actions

- (void)login {
    NSLog(@"implement login here");
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"temp_login"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)forgot {
    NSLog(@"implement forgot password here");
}

- (void)facebook {
    NSLog(@"implement facebook here");
}

- (void)registerUser {
    NSLog(@"implement register here");
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    __weak __typeof(self)weakSelf = self;
    
    if (indexPath.row == 0) {
        LoginHeaderTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[LoginHeaderTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        
        itemCell.lblTitle.text = LOCS(@"Exclusive Club");
        itemCell.lblSubTitle.text = LOCS(@"SIGN IN");
        
        cell = itemCell;
    } else if (indexPath.row == 1) {
        TextFieldTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[TextFieldTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        
        itemCell.textField.placeholder = LOCS(@"Enter e-mail");
        itemCell.textField.secureTextEntry = NO;
        
        cell = itemCell;
    } else if (indexPath.row == 2) {
        TextFieldTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[TextFieldTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        
        itemCell.textField.placeholder = LOCS(@"Enter password");
        itemCell.textField.secureTextEntry = YES;
        
        cell = itemCell;
    } else if (indexPath.row == 3) {
        LoginButtonsTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[LoginButtonsTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        
        [itemCell setButtonLoginHandler:^(LoginButtonsTableViewCell *cell) {
            [weakSelf login];
        }];
        
        [itemCell setButtonForgotHandler:^(LoginButtonsTableViewCell *cell) {
            [weakSelf forgot];
        }];
        
        [itemCell setButtonFacebookHandler:^(LoginButtonsTableViewCell *cell) {
            [weakSelf facebook];
        }];
        
        [itemCell setButtonRegisterHandler:^(LoginButtonsTableViewCell *cell) {
            [weakSelf registerUser];
        }];
        
        cell = itemCell;
    }
//    else if (indexPath.row == 1) {
//        UserInfoTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[UserInfoTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
//        itemCell.vSeparator.hidden = NO;
//        itemCell.lblDesc.text = LOCS(@"First name");
//        itemCell.lblData.text = @"David";
//        
//        cell = itemCell;
//    } else if (indexPath.row == 2) {
//        UserInfoTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[UserInfoTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
//        itemCell.vSeparator.hidden = NO;
//        itemCell.lblDesc.text = LOCS(@"Last name");
//        itemCell.lblData.text = @"Hogan";
//        
//        cell = itemCell;
//    } else if (indexPath.row == 3) {
//        UserInfoTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[UserInfoTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
//        itemCell.vSeparator.hidden = YES;
//        itemCell.lblDesc.text = LOCS(@"Customer No.");
//        itemCell.lblData.text = @"007";
//        
//        cell = itemCell;
//    } else if (indexPath.row == 4) {
//        BarcodeTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[BarcodeTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
//        
//        itemCell.ivBarcode.image = [UIImage imageNamed:@"tempbarcode"];
//        
//        cell = itemCell;
    //}
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return kLoginHeaderTableViewCellHeight;
    } else if (indexPath.row == 3) {
        return kLoginButtonsTableViewCellHeight;
    }
    return kTextFieldTableViewCellHeight;
}



@end
