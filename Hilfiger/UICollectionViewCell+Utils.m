//
//  UICollectionViewCell+Utils.m
//  Inviter
//
//  Created by Pavel Krusek on 13/01/15.
//
//

#import "UICollectionViewCell+Utils.h"

@implementation UICollectionViewCell (Utils)

+ (NSString *)defaultReuseIdentifier{
    return NSStringFromClass(self);
}

@end
