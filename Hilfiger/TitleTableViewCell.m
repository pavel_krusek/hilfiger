//
//  TitleTableViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "TitleTableViewCell.h"

CGFloat const kTitleTableViewCellHeight = 122.;

@implementation TitleTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor themeColorBackground];
        
        _lblTitle = [UILabel new];
        _lblTitle.font = [UIFont fontWithName:@"Caslon540ITALIC" size:48];
        _lblTitle.textColor = [UIColor themeColorText];
        _lblTitle.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_lblTitle];

        ALVariableBindingsAM(_lblTitle);
        
        AL_Visual_AddToView(self.contentView, @"H:|[_lblTitle]|");
        AL_CenterY_AddToView(self.contentView, _lblTitle, self.contentView);
        
        [self layoutIfNeeded];
    }
    
    return self;
}


@end
