//
//  TextFieldTableViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

FOUNDATION_EXPORT CGFloat const kTextFieldTableViewCellHeight;

#import "BaseFormTableViewCell.h"

@interface TextFieldTableViewCell : BaseFormTableViewCell

@end
