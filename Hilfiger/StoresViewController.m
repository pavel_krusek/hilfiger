//
//  StoresViewController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "StoresViewController.h"

@implementation StoresViewController

#pragma mark - Lifecycle

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setEdgesForExtendedLayout:UIRectEdgeNone];
    
    self.navigationItem.leftBarButtonItem = _btnSettings;
    
    [_btnSearch setTitle:LOCS(@"SEARCH") forState:UIControlStateNormal];
    _btnSearch.backgroundColor = [UIColor themeColorGreen];
    _btnSearch.titleLabel.font = [UIFont themeFontButton];
    _btnSearch.adjustsImageWhenHighlighted = NO;
    
    _tfSearch.layer.borderWidth = 1;
    _tfSearch.layer.borderColor = [COLOR(0xd4d4d4) CGColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
}

#pragma mark - Keyboard notifications handlers

- (void)keyboardDidShow:(NSNotification *)sender {
    NSDictionary* info = [sender userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    _inputViewBottomConstraint.constant = kbSize.height - self.tabBarController.tabBar.frame.size.height;
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];

}

- (void)keyboardWillHide:(NSNotification *)sender {
    _inputViewBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

#pragma mark - Actions

- (IBAction)actionSearch:(id)sender {
}

#pragma mark - TextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

@end
