//
//  VoucherCollectionViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "VoucherCollectionViewCell.h"

CGFloat const kVoucherCollectionViewCellCollectionViewCellHeight = 245.;

@implementation VoucherCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        _ivImage = [UIImageView new];
        _ivImage.backgroundColor = [UIColor themeColorGreen];
        _ivImage.contentMode = UIViewContentModeScaleAspectFill;
        _ivImage.clipsToBounds = YES;
        [self.contentView addSubview:_ivImage];
        
        _lblSubtitle = [UILabel new];
        _lblSubtitle.font = [UIFont themeFontSubtitle];
        _lblSubtitle.textColor = [UIColor themeColorText];
        _lblSubtitle.numberOfLines = 3;
        [self.contentView addSubview:_lblSubtitle];
        
        _ivTime = [UIImageView new];
        _ivTime.image = [UIImage imageNamed:@"time"];
        [self.contentView addSubview:_ivTime];
        
        _lblDate = [UILabel new];
        _lblDate.font = [UIFont themeFontSmall];
        _lblDate.textColor = [UIColor themeColorGreen];
        [self.contentView addSubview:_lblDate];
      
        ALVariableBindingsAM(_ivImage, _lblSubtitle, _lblDate, _ivTime);
    
        AL_Visual_AddToView(self.contentView, @"V:|-0-[_ivImage(150)]");
        AL_Visual_AddToView(self.contentView, @"H:|[_ivImage]|");
        
        AL_Visual_AddToView(self.contentView, @"V:[_ivImage]-10-[_lblSubtitle]");
        AL_Visual_AddToView(self.contentView, @"H:|-15-[_lblSubtitle]-15-|");
        
        AL_Visual_AddToView(self.contentView, @"V:[_ivTime]-10-|");
        
        AL_Visual_AddToView(self.contentView, @"V:[_lblDate]-10-|");
        AL_Visual_AddToView(self.contentView, @"H:|-15-[_ivTime]-3-[_lblDate]");
        
        [self layoutIfNeeded];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _ivImage.image = nil;
    _lblSubtitle.text = nil;
    _lblDate.text = nil;
}


@end
