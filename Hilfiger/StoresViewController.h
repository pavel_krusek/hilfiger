//
//  StoresViewController.h
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

@import MapKit;

#import "BaseViewController.h"

@interface StoresViewController : BaseViewController <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UIButton *btnSearch;
@property (nonatomic, weak) IBOutlet UITextField *tfSearch;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *inputViewBottomConstraint;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnSettings;

- (IBAction)actionSearch:(id)sender;

@end
