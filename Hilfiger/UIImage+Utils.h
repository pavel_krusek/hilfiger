//
//  UIImage+Utils.h
//  Reporterio
//
//  Created by Pavel Krusek on 11/15/14.
//  Copyright (c) 2014 Pavel Krusek. All rights reserved.
//

@interface UIImage (Utils)

+ (UIImage *)imageWithColor:(UIColor *)color;

+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size;

@end
