//
//  BaseFormTableViewCell.h
//  Hilfiger
//
//  Created by Pavel Krusek on 28/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface BaseFormTableViewCell : BaseTableViewCell <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, copy) void (^textFieldDidChangeHandler)(BaseFormTableViewCell *cell, UITextField *textField);
@property (nonatomic, copy) void (^textFieldShouldReturnHandler)(BaseFormTableViewCell *cell);
@property (nonatomic, copy) void (^textFieldDidBeginEditingHandler)(BaseFormTableViewCell *cell);
@property (nonatomic, copy) void (^textFieldDidEndEditingHandler)(BaseFormTableViewCell *cell, UITextField *textField);
@property (nonatomic, copy) void (^textFieldShouldBeginEditingHandler)(BaseFormTableViewCell *cell);


@end
