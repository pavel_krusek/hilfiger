//
//  UITableViewCell+Utils.h
//  njoy-ios
//
//  Created by Pavel Krusek on 10/21/14.
//  Copyright (c) 2014 Lithio. All rights reserved.
//

@interface UITableViewCell (Utils)

+ (NSString *)defaultReuseIdentifier;

@end
