//
//  DefaultNavigationViewController.h
//  Hilfiger
//
//  Created by Pavel Krusek on 26/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

@interface DefaultNavigationViewController : UINavigationController <UINavigationControllerDelegate>

@end
