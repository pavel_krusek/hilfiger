//
//  CardViewController.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "CardViewController.h"

#import "TitleTableViewCell.h"
#import "UserInfoTableViewCell.h"
#import "BarcodeTableViewCell.h"

#import "UITableViewCell+Utils.h"

@implementation CardViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = _btnSettings;
    
    _tableView.backgroundColor = [UIColor whiteColor];
    
    UIView *topview = [[UIView alloc] initWithFrame:CGRectMake(0,-480,_tableView.frame.size.width,480)];
    topview.backgroundColor = [UIColor themeColorBackground];
    [_tableView addSubview:topview];
    
    [_tableView registerClass:[TitleTableViewCell class] forCellReuseIdentifier:[TitleTableViewCell defaultReuseIdentifier]];
    [_tableView registerClass:[UserInfoTableViewCell class] forCellReuseIdentifier:[UserInfoTableViewCell defaultReuseIdentifier]];
    [_tableView registerClass:[BarcodeTableViewCell class] forCellReuseIdentifier:[BarcodeTableViewCell defaultReuseIdentifier]];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    if (indexPath.row == 0) {
        TitleTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[TitleTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        
        itemCell.lblTitle.text = LOCS(@"Your Club Card");
        
        cell = itemCell;
    } else if (indexPath.row == 1) {
        UserInfoTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[UserInfoTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        itemCell.vSeparator.hidden = NO;
        itemCell.lblDesc.text = LOCS(@"First name");
        itemCell.lblData.text = @"David";
        
        cell = itemCell;
    } else if (indexPath.row == 2) {
        UserInfoTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[UserInfoTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        itemCell.vSeparator.hidden = NO;
        itemCell.lblDesc.text = LOCS(@"Last name");
        itemCell.lblData.text = @"Hogan";
        
        cell = itemCell;
    } else if (indexPath.row == 3) {
        UserInfoTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[UserInfoTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        itemCell.vSeparator.hidden = YES;
        itemCell.lblDesc.text = LOCS(@"Customer No.");
        itemCell.lblData.text = @"007";
        
        cell = itemCell;
    } else if (indexPath.row == 4) {
        BarcodeTableViewCell *itemCell = [tableView dequeueReusableCellWithIdentifier:[BarcodeTableViewCell defaultReuseIdentifier] forIndexPath:indexPath];
        
        itemCell.ivBarcode.image = [UIImage imageNamed:@"tempbarcode"];
        
        cell = itemCell;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return kTitleTableViewCellHeight;
    } else if (indexPath.row == 4) {
        return kBarcodeTableViewCellHeight;
    }
    return kUserInfoTableViewCellHeight;
}

@end
