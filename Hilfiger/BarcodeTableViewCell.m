//
//  BarcodeTableViewCell.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "BarcodeTableViewCell.h"

CGFloat const kBarcodeTableViewCellHeight = 260.;

@implementation BarcodeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor themeColorBackground];
        
        _ivStripe = [UIImageView new];
        _ivStripe.contentMode = UIViewContentModeScaleAspectFill;
        _ivStripe.image = [UIImage imageNamed:@"stripe"];
        [self.contentView addSubview:_ivStripe];
        
        _vBarcodeContainer = [UIView new];
        _vBarcodeContainer.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_vBarcodeContainer];
        
        _ivBarcode = [UIImageView new];
        [_vBarcodeContainer addSubview:_ivBarcode];
        
        ALVariableBindingsAM(_ivStripe, _vBarcodeContainer, _ivBarcode);
        
        AL_Visual_AddToView(self.contentView, @"V:|-25-[_ivStripe]-0-[_vBarcodeContainer]|");
        AL_Visual_AddToView(self.contentView, @"H:|[_ivStripe]|");
    
        AL_Visual_AddToView(self.contentView, @"H:|[_vBarcodeContainer]|");
        
        AL_CenterX_AddToView(_vBarcodeContainer, _ivBarcode, _vBarcodeContainer);
        AL_CenterY_AddToView(_vBarcodeContainer, _ivBarcode, _vBarcodeContainer);
        
        [self layoutIfNeeded];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    _ivBarcode.image = nil;
}

@end
