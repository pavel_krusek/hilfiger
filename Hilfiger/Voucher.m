//
//  Voucher.m
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import "Voucher.h"

@implementation Voucher

+ (Voucher *)voucherWithImagesource:(NSString *)image description:(NSString *)description date:(NSString *)date {
    Voucher *voucher = [Voucher new];
    
    voucher.iVoucher = [UIImage imageNamed:image];
    voucher.desc = description;
    voucher.date = date;

    return voucher;
}

@end
