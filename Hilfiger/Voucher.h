//
//  Voucher.h
//  Hilfiger
//
//  Created by Pavel Krusek on 27/04/15.
//  Copyright (c) 2015 Pavel Krusek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Voucher : NSObject

@property (nonatomic, copy) UIImage *iVoucher;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *date;

+ (Voucher *)voucherWithImagesource:(NSString *)image description:(NSString *)description date:(NSString *)date;

@end
